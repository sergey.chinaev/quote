package main

import (
	"context"
	"gitlab.com/golight/dao/connector"
	"gitlab.com/golight/loggerx"
	"gitlab.com/golight/migrator"
	"gitlab.com/golight/scanner"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"os/signal"
	"quote/config"
	"quote/modules/handlers"
	"quote/modules/quotes"
	"quote/modules/repository"
	"quote/modules/router"
	"syscall"
	"time"
)

func main() {
	appConf, err := config.NewAppConfig(".env")
	if err != nil {
		log.Fatal(err)
	}

	ts := scanner.NewTableScanner()
	ent := &quotes.Quote{}
	ts.RegisterTable(ent)

	logger := loggerx.InitLogger(appConf.Name, appConf.Production)
	var sqlDB *connector.SqlDB

	sqlDB, err = connector.NewSqlDB(appConf.Db, ts, logger)
	if err != nil {
		logger.Fatal("failed to connect to db", zap.Error(err))
	}
	defer sqlDB.DB.Close()

	err = migrator.NewMigrator(sqlDB.DB, appConf.Db, ts).Migrate()
	if err != nil {
		logger.Fatal("failed to migrate", zap.Error(err))
	}

	qs := repository.NewQuotesStorage(sqlDB.DAO)
	authService := config.NewAuthService(config.AppConfigAuth(appConf.Auth))
	qh := handlers.NewQuotesHandler(authService, qs)
	r := router.NewRouter(&qs, qh, authService)

	server := http.Server{
		Addr:         ":8080",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	serverStarted := make(chan struct{})
	go func() {
		log.Println("Starting server...")
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Server error: %v", err)
		}
	}()

	go func() {
		<-serverStarted
	}()

	<-sigChan

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = server.Shutdown(ctx)
	if err != nil {
		log.Fatalf("Server shutdown error: %v", err)
	}

	log.Println("Server stopped gracefully")
}
