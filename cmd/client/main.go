package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"
)

func main() {
	jar, err := cookiejar.New(nil)
	if err != nil {
		log.Fatal(err)
	}
	client := &http.Client{
		Jar: jar,
	}
	url := "http://localhost:8080/"

	authUsername := os.Getenv("AUTH_USERNAME")
	authPassword := os.Getenv("AUTH_PASSWORD")

	url = "http://localhost:8080/authentication"
	req, err := http.NewRequest("GET", url, &bytes.Buffer{})
	if err != nil {
		log.Fatal(err)
	}
	req.SetBasicAuth(authUsername, authPassword)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(body))
	fmt.Println()

	// Adding a new quote
	url = "http://localhost:8080/quote/create"
	log.Println("Added:")

	newQuote1 := struct {
		ID     int    `json:"id"`
		Author string `json:"author"`
		Text   string `json:"text"`
	}{
		ID:     1,
		Author: "А.П.Чехов",
		Text:   "Нашего человека стоит хвалить уже за намерение",
	}

	fmt.Printf("%s: %s\n", newQuote1.Author, newQuote1.Text)

	jsonData1, err := json.Marshal(newQuote1)
	if err != nil {
		log.Fatal(err)
	}

	req, err = http.NewRequest("POST", url, bytes.NewBuffer(jsonData1))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err = io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	newQuote2 := struct {
		ID     int    `json:"id"`
		Author string `json:"author"`
		Text   string `json:"text"`
	}{
		ID:     2,
		Author: "Неизвестный психиатр",
		Text:   "Нет здоровых людей, есть необследованные",
	}

	fmt.Printf("%s: %s\n", newQuote2.Author, newQuote2.Text)

	jsonData2, err := json.Marshal(newQuote2)
	if err != nil {
		log.Fatal(err)
	}

	req, err = http.NewRequest("POST", url, bytes.NewBuffer(jsonData2))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err = io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	newQuote3 := struct {
		ID     int    `json:"id"`
		Author string `json:"author"`
		Text   string `json:"text"`
	}{
		ID:     3,
		Author: "Эрнесто Че Гевара",
		Text:   "Не люблю, когда печатают мое лицо на футболках",
	}

	fmt.Printf("%s: %s\n", newQuote3.Author, newQuote3.Text)

	jsonData3, err := json.Marshal(newQuote3)
	if err != nil {
		log.Fatal(err)
	}

	req, err = http.NewRequest("POST", url, bytes.NewBuffer(jsonData3))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err = io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	url = "http://localhost:8080/quote/random"
	log.Println(url)

	req, err = http.NewRequest("GET", url, bytes.NewBuffer(nil))
	if err != nil {
		log.Fatal(err)
	}
	resp, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	log.Println("Random quote:")
	io.Copy(os.Stdout, resp.Body)
}
