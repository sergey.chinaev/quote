package repository

import (
	"context"
	"gitlab.com/golight/dao/params"
	"quote/modules/quotes"
)

type Quoteser interface {
	Create(ctx context.Context, dto quotes.QuoteDTO) error
	Update(ctx context.Context, dto quotes.QuoteDTO) error
	GetByID(ctx context.Context, quotesID int) (quotes.QuoteDTO, error)
	GetList(ctx context.Context, condition params.Condition) ([]quotes.QuoteDTO, error)
	Delete(ctx context.Context, quotesID int) error
}
