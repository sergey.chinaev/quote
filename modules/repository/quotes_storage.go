package repository

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/golight/dao"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/types"
	"math/rand"
	"quote/modules/quotes"
	"time"
)

type QuotesStorage struct {
	adapter dao.DAOFace
}

func NewQuotesStorage(sqlAdapter dao.DAOFace) QuotesStorage {
	return QuotesStorage{adapter: sqlAdapter}
}

func (q *QuotesStorage) Create(ctx context.Context, dto quotes.QuoteDTO) error {
	//return q.adapter.Create(ctx, &dto)
	_, err := q.adapter.Create(ctx, &dto)
	return err
}

func (q *QuotesStorage) Update(ctx context.Context, dto quotes.QuoteDTO) error {
	return q.adapter.Update(
		ctx,
		&dto,
		params.Condition{
			Equal: map[string]interface{}{"id": dto.ID},
		},
		params.Update,
	)
}

func (q *QuotesStorage) GetByID(ctx context.Context, quotesID int) (quotes.QuoteDTO, error) {
	var list []quotes.QuoteDTO
	var table quotes.QuoteDTO
	err := q.adapter.List(ctx, &list, &table, params.Condition{
		Equal: map[string]interface{}{"id": quotesID},
	})
	if err != nil {
		return quotes.QuoteDTO{}, err
	}
	if len(list) < 1 {
		return quotes.QuoteDTO{}, fmt.Errorf("quotes storage: GetByID not found")
	}
	return list[0], err
}

func (q *QuotesStorage) GetList(ctx context.Context, condition params.Condition) ([]quotes.QuoteDTO, error) {
	var list []quotes.QuoteDTO
	var table quotes.QuoteDTO
	err := q.adapter.List(ctx, &list, &table, condition)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (q *QuotesStorage) Delete(ctx context.Context, quotesID int) error {
	table, err := q.GetByID(ctx, quotesID)
	if err != nil {
		return err
	}

	table.DeletedAt = types.NewNullTime(time.Now())

	return q.adapter.Update(
		ctx,
		&table,
		params.Condition{
			Equal: map[string]interface{}{"id": table.ID},
		},
		params.Update,
	)
}

func (q *QuotesStorage) GetRandom(ctx context.Context) ([]quotes.QuoteDTO, error) {
	// Получение количества цитат в хранилище
	count, err := q.adapter.GetCount(ctx, &quotes.Quote{}, params.Condition{})
	if err != nil {
		return []quotes.QuoteDTO{}, err
	}

	// Проверка, есть ли цитаты в хранилище
	if count == 0 {
		return []quotes.QuoteDTO{}, errors.New("no quotes found in the storage")
	}

	// Генерация случайного индекса для выбора цитаты
	randomIndex := rand.Intn(int(count))

	// Подготовка условия для выбора случайной цитаты
	condition := params.Condition{
		LimitOffset: &params.LimitOffset{
			Offset: int64(randomIndex),
			Limit:  1,
		},
	}

	// Получение случайной цитаты из хранилища
	var randomQuote []quotes.QuoteDTO
	err = q.adapter.List(ctx, &randomQuote, &quotes.Quote{}, condition)
	if err != nil {
		return []quotes.QuoteDTO{}, err
	}

	return randomQuote, nil
}
