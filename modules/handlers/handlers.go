package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/responder"
	"math/rand"
	"net/http"
	"quote/config"
	"quote/modules/quotes"
	"quote/modules/repository"
	"strconv"
)

type QuotesHandler struct {
	Auth *config.Auth

	Storage repository.QuotesStorage

	responder responder.Responder
	//service   service.QuoteServicer
}

func NewQuotesHandler(auth *config.Auth, qs repository.QuotesStorage) *QuotesHandler {
	return &QuotesHandler{
		Auth:    auth,
		Storage: qs,
	}
}

func (qh *QuotesHandler) CreateQuote(w http.ResponseWriter, r *http.Request) {
	var newQuote quotes.QuoteDTO
	err := json.NewDecoder(r.Body).Decode(&newQuote)
	if err != nil {
		http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	err = qh.Storage.Create(r.Context(), newQuote)
	if err != nil {
		http.Error(w, "Failed to create quote", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Quote created successfully"))

}

func (qh *QuotesHandler) GetQuote(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid quote ID", http.StatusBadRequest)
		return
	}

	quote, err := qh.Storage.GetByID(r.Context(), id)
	if err != nil {
		http.Error(w, "Failed to get quote", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(quote)
}

func (qh *QuotesHandler) UpdateQuote(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid quote ID", http.StatusBadRequest)
		return
	}

	var updatedQuote quotes.QuoteDTO
	err = json.NewDecoder(r.Body).Decode(&updatedQuote)
	if err != nil {
		http.Error(w, "Failed to decode request body", http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	updatedQuote.ID = id

	err = qh.Storage.Update(r.Context(), updatedQuote)
	if err != nil {
		http.Error(w, "Failed to update quote", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Quote updated successfully"))
}

func (qh *QuotesHandler) DeleteQuote(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		http.Error(w, "Invalid quote ID", http.StatusBadRequest)
		return
	}

	err = qh.Storage.Delete(r.Context(), id)
	if err != nil {
		http.Error(w, "Failed to delete quote", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Quote deleted successfully"))
}

func (qh *QuotesHandler) GetRandom(w http.ResponseWriter, r *http.Request) {
	//randomQuote, err := qh.Storage.GetRandom(r.Context())
	//if err != nil {
	//	http.Error(w, fmt.Sprintf("Failed to get random quote: %v", err), http.StatusInternalServerError)
	//	return
	//}
	//
	//w.Header().Set("Content-Type", "application/json")
	//json.NewEncoder(w).Encode(randomQuote)
	randomQuote, err := qh.Storage.GetRandom(r.Context())
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to get random quote: %v", err), http.StatusInternalServerError)
		return
	}

	// Выбираем случайную цитату из среза
	randomIndex := rand.Intn(len(randomQuote))
	selectedQuote := randomQuote[randomIndex]

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(selectedQuote)
}

func (qh *QuotesHandler) Authentication(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	err := qh.Auth.Validate(username, password)
	if err != nil {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Authenticated"))
}

func (qh *QuotesHandler) GetList(w http.ResponseWriter, r *http.Request) {
	quotesList, err := qh.Storage.GetList(r.Context(), params.Condition{})
	if err != nil {
		http.Error(w, "Failed to get quotes list", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(quotesList)
}
