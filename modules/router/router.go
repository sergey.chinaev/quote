package router

import (
	"github.com/go-chi/chi"
	"net/http"
	"quote/config"
	"quote/modules/handlers"
	"quote/modules/repository"
)

func NewRouter(qs *repository.QuotesStorage, qh *handlers.QuotesHandler, authService *config.Auth) *chi.Mux {
	router := chi.NewRouter()

	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Сервис цитат"))
	})
	router.Post("/quote/create", qh.CreateQuote)
	router.Get("/quote/get/{id}", qh.GetQuote)
	router.Put("/quote/update/{id}", qh.UpdateQuote)
	router.Put("/quote", qh.GetList)
	router.Delete("/quote/delete/{id}", qh.DeleteQuote)
	router.Get("/quote/random", qh.GetRandom)
	router.Get("/quote/list", qh.GetList)
	router.With(Authentication(authService)).Get("/authentication", qh.Authentication)

	return router

}

func Authentication(authService *config.Auth) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			username, password, ok := r.BasicAuth()
			if !ok {
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				return
			}

			err := authService.Validate(username, password)
			if err != nil {
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				return
			}

			next.ServeHTTP(w, r)
		})
	}
}
