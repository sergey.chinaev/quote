#!/bin/bash

docker-compose down

docker-compose up -d

cd ./cmd/server
go run main.go &
