package config

import (
	"errors"
)

type Auth struct {
	Username string
	Password string
}

func NewAuthService(authConf AppConfigAuth) *Auth {
	return &Auth{
		Username: authConf.Username,
		Password: authConf.Password,
	}
}

// Validate проверяет предоставленные учетные данные на соответствие сохраненным в сервисе
func (as *Auth) Validate(username, password string) error {
	if username != as.Username || password != as.Password {
		return errors.New("invalid credentials")
	}
	return nil
}
